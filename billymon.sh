#!/bin/bash
# I have no idea what I'm doing.
# TODO: This project is a piece of shit and really inefficient.

# Change directory relative to the current file path.
cd $(dirname "$(readlink -f "$0")")

# Check if files are in memory.
if [ -d "/dev/shm/billymon" ]; then
    echo "BillyMon is already running or did not shut down properly. Directory exists at: /dev/shm/billymon"
    exit 1
fi

# Run function before the program exits.
function exit {
    echo "Cleaning up."
    JOBS="$(jobs -p)";
    if [ -n "${JOBS}" ]; then
        kill ${JOBS}
    fi

    echo "Retrieving files from memory."
    mv /dev/shm/billymon current
}
trap exit EXIT

# Check if possible to resume from last time.
if [ -d "current" ]; then
    echo "Resuming from last time."
else
    cp -r src current
fi

echo "Moving files to memory."
mv current /dev/shm/billymon

echo "Entering loop."
while :
do
    timeout -v --kill-after=60s 60s bash -c "
        cd /dev/shm/billymon/scripts
        bash cpu.sh &
        bash ping.sh &
        bash network.sh &
        bash memory.sh &
        wait
    " &
    sleep 30 
    # I don't really know why this is here, something something jobs table gradually increasing over time. I don't care to investigate any further.
    disown -a
done