#!/bin/bash
rrdtool create memory.rrd \
    --step 30 \
    DS:used:GAUGE:60:0:U \
    DS:free:GAUGE:60:0:U \
    DS:page:GAUGE:60:0:U \
    RRA:AVERAGE:0.5:2:1440