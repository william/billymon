#!/bin/sh

OUTPUT=$(free -b | grep Mem)
USED=$(echo $OUTPUT | awk '{print $3}')
FREE=$(echo $OUTPUT | awk '{print $4}')
PAGE=$(echo $OUTPUT | awk '{print $6}')

rrdtool update memory.rrd -t used:free:page N:${USED}:${FREE}:${PAGE}