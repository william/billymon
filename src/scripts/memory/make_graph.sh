#!/bin/bash
rrdtool graph "memory.png" \
    -s now-24h -e now \
    -w 700 -h 300 \
    -l 0 \
    --border 0 \
    --color BACK#00000000 \
    -v "Memory usage [Gigabytes]" \
    DEF:used=memory.rrd:used:AVERAGE \
    DEF:free=memory.rrd:free:AVERAGE \
    DEF:page=memory.rrd:page:AVERAGE \
    LINE1:used#FF0000 \
    LINE1:page#0000FF::STACK \
    LINE1:free#00FF00::STACK \
    AREA:used#F005 \
    AREA:page#00F5:STACK \
    AREA:free#0F05:STACK \
    LINE0:free#00FF00:"Free\\:      " \
    "GPRINT:free:MIN:%6.1lf%s Min,\g" \
	"GPRINT:free:AVERAGE:%6.1lf%s Avg,\g" \
    "GPRINT:free:MAX:%6.1lf%s Max,\g" \
	"GPRINT:free:LAST:%6.1lf%s Last\\n" \
    LINE0:page#0000FF:"Page cache\\:" \
    "GPRINT:page:MIN:%6.1lf%s Min,\g" \
	"GPRINT:page:AVERAGE:%6.1lf%s Avg,\g" \
    "GPRINT:page:MAX:%6.1lf%s Max,\g" \
	"GPRINT:page:LAST:%6.1lf%s Last\\n" \
    LINE0:used#FF0000:"Used\\:      " \
    "GPRINT:used:MIN:%6.1lf%s Min,\g" \
	"GPRINT:used:AVERAGE:%6.1lf%s Avg,\g" \
    "GPRINT:used:MAX:%6.1lf%s Max,\g" \
	"GPRINT:used:LAST:%6.1lf%s Last\\n" >/dev/null

mv *.png ../../public