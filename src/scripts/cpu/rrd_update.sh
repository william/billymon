#!/bin/bash
if [[ ! -f "previousStats.txt" ]]; then
    cat /proc/stat > previousStats.txt
    sleep 0.2
fi

previousStats=$(cat previousStats.txt)

cat /proc/stat > previousStats.txt

currentStats=$(cat /proc/stat)

currentLine=$(echo "$currentStats" | grep "cpu ")
user=$(echo "$currentLine" | awk -F " " '{print $2}')
nice=$(echo "$currentLine" | awk -F " " '{print $3}')
system=$(echo "$currentLine" | awk -F " " '{print $4}')
idle=$(echo "$currentLine" | awk -F " " '{print $5}')
iowait=$(echo "$currentLine" | awk -F " " '{print $6}')
irq=$(echo "$currentLine" | awk -F " " '{print $7}')
softirq=$(echo "$currentLine" | awk -F " " '{print $8}')
steal=$(echo "$currentLine" | awk -F " " '{print $9}')

previousLine=$(echo "$previousStats" | grep "cpu ")
prevuser=$(echo "$previousLine" | awk -F " " '{print $2}')
prevnice=$(echo "$previousLine" | awk -F " " '{print $3}')
prevsystem=$(echo "$previousLine" | awk -F " " '{print $4}')
previdle=$(echo "$previousLine" | awk -F " " '{print $5}')
previowait=$(echo "$previousLine" | awk -F " " '{print $6}')
previrq=$(echo "$previousLine" | awk -F " " '{print $7}')
prevsoftirq=$(echo "$previousLine" | awk -F " " '{print $8}')
prevsteal=$(echo "$previousLine" | awk -F " " '{print $9}')

PrevIdle=$((previdle + previowait))
Idle=$((idle + iowait))
PrevNonIdle=$((prevuser + prevnice + prevsystem + previrq + prevsoftirq + prevsteal))
NonIdle=$((user + nice + system + irq + softirq + steal))

PrevTotal=$((PrevIdle + PrevNonIdle))
Total=$((Idle + NonIdle))

totald=$((Total - PrevTotal))
idled=$((Idle - PrevIdle))



US=$(awk "BEGIN {print ($user - $prevuser)/$totald*100}")
SY=$(awk "BEGIN {print ($system - $prevsystem)/$totald*100}")
NI=$(awk "BEGIN {print ($nice - $prevnice)/$totald*100}")
WA=$(awk "BEGIN {print ($iowait - $previowait)/$totald*100}")
HI=$(awk "BEGIN {print ($irq - $previrq)/$totald*100}")
SI=$(awk "BEGIN {print ($softirq - $prevsoftirq)/$totald*100}")
ST=$(awk "BEGIN {print ($steal - $prevsteal)/$totald*100}")

#TOTAL=$(awk "BEGIN {print ($totald - $idled)/$totald*100}")
#OTHER=$(awk "BEGIN {print ($HI + $SI + $ST)}")

rrdtool update cpu.rrd -t us:sy:wa N:${US}:${SY}:${WA}