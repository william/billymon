#!/bin/bash
rrdtool create cpu.rrd \
    --step 30 \
    DS:us:GAUGE:60:0:100 \
    DS:sy:GAUGE:60:0:100 \
    DS:wa:GAUGE:60:0:100 \
    RRA:AVERAGE:0.5:2:1440