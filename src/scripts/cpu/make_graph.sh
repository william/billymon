#!/bin/bash
rrdtool graph "cpu.png" \
    -s now-24h -e now \
    -w 700 -h 300 \
    -l 0 -u 100 \
    --border 0 \
    --color BACK#00000000 \
    -v "CPU usage" \
    DEF:us=cpu.rrd:us:AVERAGE \
    DEF:sy=cpu.rrd:sy:AVERAGE \
    DEF:wa=cpu.rrd:wa:AVERAGE \
    LINE1:wa#F0A000 \
    LINE1:sy#FF0000::STACK \
    LINE1:us#0000FF::STACK \
    AREA:wa#F0A00055 \
    AREA:sy#F005:STACK \
    AREA:us#00F5:STACK \
    LINE0:us#0000FF:"User\\:   " \
    "GPRINT:us:MIN:%6.1lf Min,\g" \
	"GPRINT:us:AVERAGE:%6.1lf Avg,\g" \
    "GPRINT:us:MAX:%6.1lf Max,\g" \
	"GPRINT:us:LAST:%6.1lf Last\\n" \
    LINE0:sy#FF0000:"System\\: " \
    "GPRINT:sy:MIN:%6.1lf Min,\g" \
	"GPRINT:sy:AVERAGE:%6.1lf Avg,\g" \
    "GPRINT:sy:MAX:%6.1lf Max,\g" \
	"GPRINT:sy:LAST:%6.1lf Last\\n" \
    LINE0:wa#F0A000:"Wait-IO\\:" \
    "GPRINT:wa:MIN:%6.1lf Min,\g" \
	"GPRINT:wa:AVERAGE:%6.1lf Avg,\g" \
    "GPRINT:wa:MAX:%6.1lf Max,\g" \
	"GPRINT:wa:LAST:%6.1lf Last\\n" >/dev/null

mv *.png ../../public