#!/bin/bash
INTERFACE="enp0s31f6"

grep -q "^$INTERFACE:" /proc/net/dev || exec echo "$INTERFACE: no such device"

if [[ ! -f "previousRxBytes.txt" && ! -f "previousTxBytes.txt" && ! -f "previousDate.txt" ]]; then
    cat /sys/class/net/$INTERFACE/statistics/rx_bytes > previousRxBytes.txt
    cat /sys/class/net/$INTERFACE/statistics/tx_bytes > previousTxBytes.txt 
    echo "$(date '+%s.%N')" > previousDate.txt 
fi

PREV_RX=$(cat previousRxBytes.txt)
PREV_TX=$(cat previousTxBytes.txt)
NEW_RX=$(cat /sys/class/net/$INTERFACE/statistics/rx_bytes)
NEW_TX=$(cat /sys/class/net/$INTERFACE/statistics/tx_bytes)
DATE_DIFF=$(echo "($(date '+%s.%N') - $(cat previousDate.txt))" | bc -l)

RX_BPS=$(echo "($NEW_RX - $PREV_RX) * 8 / $DATE_DIFF" | bc)
TX_BPS=$(echo "($NEW_TX - $PREV_TX) * 8 / $DATE_DIFF" | bc)


cat /sys/class/net/$INTERFACE/statistics/rx_bytes > previousRxBytes.txt
cat /sys/class/net/$INTERFACE/statistics/tx_bytes > previousTxBytes.txt 
echo "$(date '+%s.%N')" > previousDate.txt 

rrdtool update network.rrd -t in:out N:${RX_BPS}:${TX_BPS}