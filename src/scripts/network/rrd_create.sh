#!/bin/bash
rrdtool create network.rrd \
    --step 30 \
    DS:in:GAUGE:60:0:U \
    DS:out:GAUGE:60:0:U \
    RRA:AVERAGE:0.5:2:1440