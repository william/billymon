#!/bin/bash
rrdtool graph "network.png" \
    -s now-24h -e now \
    -w 700 -h 300 \
    -l 0 \
    --border 0 \
    --color BACK#00000000 \
    -v "Network traffic [bits/sec]" \
    DEF:in=network.rrd:in:AVERAGE \
    DEF:out=network.rrd:out:AVERAGE \
    CDEF:out_neg=out,-1,* \
    LINE1:in#00FF00:Incoming\\: \
    AREA:in#00FF00 \
    "GPRINT:in:MIN:%6.1lf%s Min,\g" \
	"GPRINT:in:AVERAGE:%6.1lf%s Avg,\g" \
    "GPRINT:in:MAX:%6.1lf%s Max,\g" \
	"GPRINT:in:LAST:%6.1lf%s Last\\n" \
    LINE1:out_neg#0000FF:Outgoing\\: \
    AREA:out_neg#0000FF \
    "GPRINT:out:MIN:%6.1lf%s Min,\g" \
	"GPRINT:out:AVERAGE:%6.1lf%s Avg,\g" \
    "GPRINT:out:MAX:%6.1lf%s Max,\g" \
	"GPRINT:out:LAST:%6.1lf%s Last\\n" \
    HRULE:0#FF000055 >/dev/null

mv *.png ../../public