#!/bin/bash
rrdtool create ping.rrd \
    --step 30 \
    DS:ping:GAUGE:60:0:1000 \
    RRA:AVERAGE:0.5:2:1440