#!/bin/bash
rrdtool graph "ping.png" \
    -s now-24h -e now \
    -w 700 -h 300 \
    -l 0 \
    --border 0 \
    --color BACK#00000000 \
    -v "WAN ping" \
    DEF:ping=ping.rrd:ping:AVERAGE \
    LINE1:ping#00FF00:Ping\\: \
    AREA:ping#0F05 \
    "GPRINT:ping:MIN: %2.1lf ms Min," \
    "GPRINT:ping:AVERAGE:%2.1lf ms Avg," \
    "GPRINT:ping:MAX:%2.1lf ms Max," \
    "GPRINT:ping:LAST:%2.1lf ms Last\\n" >/dev/null

mv *.png ../../public