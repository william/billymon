#!/bin/bash
HOST="84.212.0.1"

PING=$(ping -i 0.2 -w 30 $HOST | awk -F '/' 'END {print $5}')
if [ -z "$PING" ]; then
    PING="U"
fi

rrdtool update ping.rrd N:${PING}