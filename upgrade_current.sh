#!/bin/bash

cd $(dirname "$(readlink -f "$0")")

if [ -d "current" ]; then
    cp -r src/* current/
else
    echo "\"current\" directory was not found, did you stop the service?"
fi