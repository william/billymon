#!/bin/bash

cd $(dirname "$(readlink -f "$0")")

if [ -d "/dev/shm/billymon" ]; then
    rm -r /dev/shm/billymon
fi

if [ -d "current" ]; then
    rm -r current
fi