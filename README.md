# BillyMon

Monitoring scripts for creating graphs with RRDTool or something like that.

# Installation

A not so very detailed guide on how to install and configure this on your system.

## Prerequisites

Tested working on new Debian 10 Buster installation.

Install the required packages:

    apt install rrdtool coreutils iputils-ping git

## Install

Clone the repo:

    cd /opt/
    git clone <REPOSITORY URL GOES HERE>
    cd <REPOSITORY NAME GOES HERE>

## Configuration 

You can change a few things like the host for the ping graph and the network interface for the network graph.

    $ cat src/scripts/network/rrd_update.sh

    #!/bin/bash
    INTERFACE="enp0s31f6"
    ...

    $ cat src/scripts/ping/rrd_update.sh

    #!/bin/bash
    HOST="84.212.0.1" 
    ...

Do the same for any other graphs that seem configurable.

## Running

You can now either start billymon via the terminal:

    bash billymon.sh

Or use the preconfigured systemd service file:

    cp billymon.service /etc/systemd/system/billymon.service
    systemctl daemon-reload
    systemctl enable billymon.service --now

Remember to run upgrade_current.sh if you changed the configuration after running and want to apply it.

## Accessing the graphs

While running, everything should be located in the /dev/shm/billymon/public directory.

You can point your web server to that location.

Good luck.